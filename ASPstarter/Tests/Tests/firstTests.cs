﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using firstASP;
using firstASP.Controllers;
using Microsoft.AspNet.Mvc;
using firstASP.Data;
using firstASP.Models;

namespace Tests
{
    public class firstTests
    {
        IDataBase<Animal> db;
        public firstTests()
        {
            db = AnimalsSingletonDataBase.Instance;
        }
        
        [Fact]
        public void HomeController()
        {
            HomeController sut = new HomeController();
            Assert.True(sut.About() is IActionResult);
            
        }
        [Fact]
        public void TestDataBaseAdd()
        {
            var sut = AnimalsSingletonDataBase.Instance;
            var prevNumber = sut.GetAll().Count();
            sut.Add(new firstASP.Models.Animal() {Id=666, Name="Gówno" });
            var newCount = sut.GetAll().Count();
            Assert.True(prevNumber + 1 == newCount);
        }



        [Fact]
       public void TestAddAnimal()
        {
            var prevNumber = db.GetAll().Count();

            var sut = new AnimalsController();
            sut.Add(new Animal() { Id = 999, Name = "Kupa" });

            var newCount = db.GetAll().Count();

            Assert.True(prevNumber + 1 == newCount);
        }
    }
}
