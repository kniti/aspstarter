﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace firstASP.Data
{
    public class AnimalsSingletonDataBase:AnimalsDataBase
    {
        private AnimalsSingletonDataBase() { }
       static AnimalsSingletonDataBase instance;
        public static AnimalsSingletonDataBase Instance {
            get {
                if (instance == null)
                {
                    instance = new AnimalsSingletonDataBase();
                }
                return instance;
            }
        }
        
    }
}
