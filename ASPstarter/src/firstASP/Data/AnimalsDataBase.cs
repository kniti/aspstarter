﻿using firstASP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace firstASP.Data
{
    public class AnimalsDataBase : IDataBase<Animal>
    {
        protected List<Animal> animals;
       public AnimalsDataBase()
        {
            animals=seed();
        }
        protected List<Animal> seed()
        {
            var res = new List<Animal>();
            res.AddRange(
                new List<Animal>() {
                new Animal { Id=0, Name="Pieseł" },
                new Animal { Id=1, Name="Kot" },
                new Animal { Id=2, Name="Koń" },
                new Animal { Id=3, Name="Żaba" },
                new Animal { Id=4, Name="Komar" },
                new Animal { Id=5, Name="Delfin" }

                });
            return res;
        }
        public void Add(Animal item)
        {
            animals.Add(item);
            item.Id = animals.Count;
        }

        public Animal Get(int id)
        {
            return animals.Find(m => m.Id == id);
        }

        public IEnumerable<Animal> GetAll()
        {
            return animals;
        }

        public void Remove(int id)
        {
            animals.RemoveAll(m => m.Id == id);

        }

        public void Remove(Animal item)
        {
            animals.Remove(item);
        }

        public void Update(Animal item)
        {
            var old= animals.Find(m => m.Id == item.Id);
            var index = animals.IndexOf(old);
            animals[index] = item;
            

        }
    }
}
