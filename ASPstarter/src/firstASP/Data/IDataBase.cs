﻿using System.Collections.Generic;

namespace firstASP.Data
{
    public interface IDataBase<T>
    {
        void Add(T item);
        T Get(int id);
        IEnumerable<T> GetAll();
        void Update(T item);
        void Remove(T item);
        void Remove(int id);



    }
}