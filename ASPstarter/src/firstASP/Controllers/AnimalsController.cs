using Microsoft.AspNet.Mvc;
using firstASP.Models;
using firstASP.Data;

namespace firstASP.Controllers
{
    public class AnimalsController : Controller
    {
       IDataBase<Animal> db;
        public AnimalsController()
        {
            db = AnimalsSingletonDataBase.Instance;
        }
        public IActionResult Index()
        {

            var animals = db.GetAll();
            return View(animals);
        }
    }
}
