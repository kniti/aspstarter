﻿using Microsoft.AspNet.Mvc;
using firstASP.Data;
using firstASP.Models;

namespace firstASP.Controllers
{
    public class HomeController : Controller
    {
        IDataBase<Animal> db;
        public HomeController()
        {
            db = AnimalsSingletonDataBase.Instance;
        }
       
        public IActionResult Index()
        {
            
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Info(string imie)
        {

            ViewBag.Imie = imie;
           return View();
        } 
        public IActionResult Kod(string kod)
        {

            ViewBag.Kod = kod;
            return View();
        }

        public string Text()
        {
            return "asdjnaskjdnaskjdnaskjd";
        }

        public IActionResult Over18(DaneUsera user)
        {
            if (user.Imie == null)
            {
               return View("UserForm",user);
            }

            if (user.Wiek < 18)
            {

                return View("ZaMlody");
            }
            else
            {

                return View();

            }
        }

    }
}
